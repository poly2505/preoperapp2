package com.g2movil1_misiontic.preoperapp2.model.entity;

public class User {
// atributos de la clase

    private Integer userid;
    private String username;
    private String email;
    private String password;
    private String rol;
    private String placa;

// constructor de la clase con el mismo nombre
    public User(Integer userid, String username, String email, String password, String rol, String placa) {
        this.userid = userid;
        this.username = username;
        this.email = email;
        this.password = password;
        this.rol = rol;
        this.placa = placa;
    }
// getter (metodo de la clase)
    public Integer getUserid() {
    return userid;
}

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getRol() {
        return rol;
    }

    public String getPlaca() {
        return placa;
    }
}
