package com.g2movil1_misiontic.preoperapp2.model.entity;

public class Inspecciones {
    private String Placa;

    private String Fecha;

    private String Aire_Acondicionado;

    private String Alarma_de_testigo;

    private String Cinturon_de_seguridad;

    private String Hora;

    private String Iluminacion_de_cabina;

    private  String Indicadores_luces_direccionales;

    private  String Novedad;

    private  String Pito_Auxiliar;

    private  String Pito_Electrico;

    private  String Plumillas_limpiavidrios;

    public void setPlaca(String placa) {
        Placa = placa;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public void setAire_Acondicionado(String aire_Acondicionado) {
        Aire_Acondicionado = aire_Acondicionado;
    }

    public void setAlarma_de_testigo(String alarma_de_testigo) {
        Alarma_de_testigo = alarma_de_testigo;
    }

    public void setCinturon_de_seguridad(String cinturon_de_seguridad) {
        Cinturon_de_seguridad = cinturon_de_seguridad;
    }

    public void setHora(String hora) {
        Hora = hora;
    }

    public void setIluminacion_de_cabina(String iluminacion_de_cabina) {
        Iluminacion_de_cabina = iluminacion_de_cabina;
    }

    public void setIndicadores_luces_direccionales(String indicadores_luces_direccionales) {
        Indicadores_luces_direccionales = indicadores_luces_direccionales;
    }

    public void setNovedad(String novedad) {
        Novedad = novedad;
    }

    public void setPito_Auxiliar(String pito_Auxiliar) {
        Pito_Auxiliar = pito_Auxiliar;
    }

    public void setPito_Electrico(String pito_Electrico) {
        Pito_Electrico = pito_Electrico;
    }

    public void setPlumillas_limpiavidrios(String plumillas_limpiavidrios) {
        Plumillas_limpiavidrios = plumillas_limpiavidrios;
    }

    public String getPlaca() {
        return Placa;
    }

    public String getFecha() {
        return Fecha;
    }

    public String getAire_Acondicionado() {
        return Aire_Acondicionado;
    }

    public String getAlarma_de_testigo() {
        return Alarma_de_testigo;
    }

    public String getCinturon_de_seguridad() {
        return Cinturon_de_seguridad;
    }

    public String getHora() {
        return Hora;
    }

    public String getIluminacion_de_cabina() {
        return Iluminacion_de_cabina;
    }

    public String getIndicadores_luces_direccionales() {
        return Indicadores_luces_direccionales;
    }

    public String getNovedad() {
        return Novedad;
    }

    public String getPito_Auxiliar() {
        return Pito_Auxiliar;
    }

    public String getPito_Electrico() {
        return Pito_Electrico;
    }

    public String getPlumillas_limpiavidrios() {
        return Plumillas_limpiavidrios;
    }

    @Override
    public String toString() {
        return "Inspecciones{" +
                "Placa='" + Placa + '\'' +
                ", Fecha='" + Fecha + '\'' +
                ", Aire_Acondicionado='" + Aire_Acondicionado + '\'' +
                ", Alarma_de_testigo='" + Alarma_de_testigo + '\'' +
                ", Cinturon_de_seguridad='" + Cinturon_de_seguridad + '\'' +
                ", Hora='" + Hora + '\'' +
                ", Iluminacion_de_cabina='" + Iluminacion_de_cabina + '\'' +
                ", Indicadores_luces_direccionales='" + Indicadores_luces_direccionales + '\'' +
                ", Novedad='" + Novedad + '\'' +
                ", Pito_Auxiliar='" + Pito_Auxiliar + '\'' +
                ", Pito_Electrico='" + Pito_Electrico + '\'' +
                ", Plumillas_limpiavidrios='" + Plumillas_limpiavidrios + '\'' +
                '}';
    }
}
