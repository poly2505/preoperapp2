package com.g2movil1_misiontic.preoperapp2.model.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.g2movil1_misiontic.preoperapp2.model.entity.Inspecciones;
import com.g2movil1_misiontic.preoperapp2.view.ViewReport.ViewReportAdapter;
import com.g2movil1_misiontic.preoperapp2.view.ViewReport.ViewReportMVP;
import com.g2movil1_misiontic.preoperapp2.view.ViewReport.ViewReportPresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class ViewReportRepository implements ViewReportMVP.Model {

    private static final String TAG = "ViewReportRepository";
    private ViewReportPresenter  viewReportPresenter;
    private String placaId;
    private DatabaseReference myRef = null;
    public String idCounter;
    RecyclerView rvCharges;



 public ViewReportRepository(Context context, ViewReportPresenter viewReportPresenter) {
     this.viewReportPresenter = viewReportPresenter;
     //locationRepository = new LocationRepository(context);

     //SalesDatabase db = SalesDatabase.getInstance(context);
     //saleDao = db.saleDao();

     myRef = FirebaseDatabase.getInstance().getReference();
     getIdCounter();
 }

    private void getIdCounter() {
   /*     myRef.child("Inspecciones").child("HSE123").child("19, August del 2021_9:11").get()
                .addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "Error recibiendo datos", task.getException());
                        } else {
                            idCounter = task.getResult().getValue(String.class);
                        }
                    }
                });*/
    }

    @Override
    public void loadData() {


        myRef.child("Inspecciones").child(placaId).get()
                .addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "Error recibiendo datos", task.getException());
                        } else {

                            List<ChargeInfo> response = new ArrayList<>();
                            Log.d("TAG", String.valueOf(task.getResult().getValue()));
                            Log.d("TAG", "Trae estos elementos: " + task.getResult().getChildrenCount());
                            for (DataSnapshot snapshot : task.getResult().getChildren()) {
                                Log.d("TAG", "Elemento: " + snapshot);
                                Inspecciones inspeccion = snapshot.getValue(Inspecciones.class);
                                Log.d("TAG", "Inspeccion: " + inspeccion);
                                response.add(new ChargeInfo(inspeccion.getPlaca(),inspeccion.getFecha(),inspeccion.getHora()));

                            }

                            Log.d("TAG","como carga el reponse: "+response);

                            viewReportPresenter.loadCharges(response);

                            //ViewReportAdapter adapter = new ViewReportAdapter(response);
                            //rvCharges.setAdapter(adapter);

                        }
                    }

                });
    }

    @Override
    public void savePlaca(String placaNow) {
        placaId = placaNow;
    }

}
