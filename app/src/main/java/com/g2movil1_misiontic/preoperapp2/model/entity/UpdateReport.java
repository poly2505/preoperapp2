package com.g2movil1_misiontic.preoperapp2.model.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity
public class UpdateReport {
    @PrimaryKey
    private Integer id;

    @ColumnInfo(name = "update_id")
    private Integer updateId;
    @ColumnInfo(name = "day_of_inspection")
    /*private Date date; */
    private String elementReport;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Integer updateId) {
        this.updateId = updateId;
    }
/*
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
*/
    public String getElementReport() {
        return elementReport;
    }

    public void setElementReport(String elementReport) {
        this.elementReport = elementReport;
    }
}
