package com.g2movil1_misiontic.preoperapp2.model.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Inspections {
    @PrimaryKey
    private Integer uid;
    private String nibs;
    private String nibOk;
    private String nibBad;
    private String whistle;
    private String whistleOk;
    private String whistleBad;
    private String alarm;
    private String alarmOk;
    private String alarmBad;
    private String auxWhistle;
    private String auxWhistleOk;
    private String auxWhistleBad;
    private String belt;
    private String beltOk;
    private String beltBad;
    private String lighting;
    private String lightingOk;
    private String lightingBad;
    private String airConditioning;
    private String airConditioningOk;
    private String airConditioningBad;
    private String indicatorss;
    private String indicatorsOk;
    private String indicatorsBad;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getNibs() {
        return nibs;
    }

    public void setNibs(String nibs) {
        this.nibs = nibs;
    }

    public String getNibOk() {
        return nibOk;
    }

    public void setNibOk(String nibOk) {
        this.nibOk = nibOk;
    }

    public String getNibBad() {
        return nibBad;
    }

    public void setNibBad(String nibBad) {
        this.nibBad = nibBad;
    }

    public String getWhistle() {
        return whistle;
    }

    public void setWhistle(String whistle) {
        this.whistle = whistle;
    }

    public String getWhistleOk() {
        return whistleOk;
    }

    public void setWhistleOk(String whistleOk) {
        this.whistleOk = whistleOk;
    }

    public String getWhistleBad() {
        return whistleBad;
    }

    public void setWhistleBad(String whistleBad) {
        this.whistleBad = whistleBad;
    }

    public String getAlarm() {
        return alarm;
    }

    public void setAlarm(String alarm) {
        this.alarm = alarm;
    }

    public String getAlarmOk() {
        return alarmOk;
    }

    public void setAlarmOk(String alarmOk) {
        this.alarmOk = alarmOk;
    }

    public String getAlarmBad() {
        return alarmBad;
    }

    public void setAlarmBad(String alarmBad) {
        this.alarmBad = alarmBad;
    }

    public String getAuxWhistle() {
        return auxWhistle;
    }

    public void setAuxWhistle(String auxWhistle) {
        this.auxWhistle = auxWhistle;
    }

    public String getAuxWhistleOk() {
        return auxWhistleOk;
    }

    public void setAuxWhistleOk(String auxWhistleOk) {
        this.auxWhistleOk = auxWhistleOk;
    }

    public String getAuxWhistleBad() {
        return auxWhistleBad;
    }

    public void setAuxWhistleBad(String auxWhistleBad) {
        this.auxWhistleBad = auxWhistleBad;
    }

    public String getBelt() {
        return belt;
    }

    public void setBelt(String belt) {
        this.belt = belt;
    }

    public String getBeltOk() {
        return beltOk;
    }

    public void setBeltOk(String beltOk) {
        this.beltOk = beltOk;
    }

    public String getBeltBad() {
        return beltBad;
    }

    public void setBeltBad(String beltBad) {
        this.beltBad = beltBad;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public String getLightingOk() {
        return lightingOk;
    }

    public void setLightingOk(String lightingOk) {
        this.lightingOk = lightingOk;
    }

    public String getLightingBad() {
        return lightingBad;
    }

    public void setLightingBad(String lightingBad) {
        this.lightingBad = lightingBad;
    }

    public String getAirConditioning() {
        return airConditioning;
    }

    public void setAirConditioning(String airConditioning) {
        this.airConditioning = airConditioning;
    }

    public String getAirConditioningOk() {
        return airConditioningOk;
    }

    public void setAirConditioningOk(String airConditioningOk) {
        this.airConditioningOk = airConditioningOk;
    }

    public String getAirConditioningBad() {
        return airConditioningBad;
    }

    public void setAirConditioningBad(String airConditioningBad) {
        this.airConditioningBad = airConditioningBad;
    }

    public String getIndicatorss() {
        return indicatorss;
    }

    public void setIndicatorss(String indicatorss) {
        this.indicatorss = indicatorss;
    }

    public String getIndicatorsOk() {
        return indicatorsOk;
    }

    public void setIndicatorsOk(String indicatorsOk) {
        this.indicatorsOk = indicatorsOk;
    }

    public String getIndicatorsBad() {
        return indicatorsBad;
    }

    public void setIndicatorsBad(String indicatorsBad) {
        this.indicatorsBad = indicatorsBad;
    }
}
