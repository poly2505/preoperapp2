package com.g2movil1_misiontic.preoperapp2.model.repository;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

public class LocationRepository {

    private FusedLocationProviderClient fusedLocationClient;
    private static Location currentLocation;


    @SuppressLint("MissingPermission")
    public LocationRepository(Context context) {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);


        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            currentLocation = location;
                            Log.i("LocationRepository", "Ubicación tomada");
                        }
                    }
                });

    }

    @SuppressLint("MissingPermission")
    public void addLocationUpdateCallback(LocationCallback callback) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(20 * 1000);

        fusedLocationClient.requestLocationUpdates(locationRequest, callback, null);
    }
    public Location getCurrentLocation() {

        return currentLocation;
    }

    public static interface LocationUpdateCallback {
        void locationUpdated(Location location);
    }
}
