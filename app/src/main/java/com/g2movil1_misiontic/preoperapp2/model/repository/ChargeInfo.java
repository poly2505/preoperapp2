package com.g2movil1_misiontic.preoperapp2.model.repository;

public class ChargeInfo {

    private  String Placa;
    private String Fecha;
    private String Hora;

    public ChargeInfo(String Placa, String Fecha, String Hora){

        this.Placa = Placa;
        this.Fecha = Fecha;
        this.Hora = Hora;
    }

    public String getPlaca() {
        return Placa;
    }

    public String getFecha() {
        return Fecha;
    }

    public String getHora() {
        return Hora;
    }
}
