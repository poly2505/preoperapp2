package com.g2movil1_misiontic.preoperapp2.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import com.g2movil1_misiontic.preoperapp2.model.entity.Inspections;

@Dao
public interface InspectionsDao {

    @Insert
    void insert(Inspections inspections);
}
