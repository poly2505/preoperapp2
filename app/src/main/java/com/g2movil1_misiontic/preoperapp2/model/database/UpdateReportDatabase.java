package com.g2movil1_misiontic.preoperapp2.model.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.g2movil1_misiontic.preoperapp2.model.dao.InspectionsDao;
import com.g2movil1_misiontic.preoperapp2.model.dao.UpdateReportDao;
import com.g2movil1_misiontic.preoperapp2.model.entity.Inspections;
import com.g2movil1_misiontic.preoperapp2.model.entity.UpdateReport;

@Database(entities ={Inspections.class, UpdateReport.class}, version = 1)
public abstract class UpdateReportDatabase extends RoomDatabase {
    public abstract InspectionsDao inspeccionDao();
    public abstract UpdateReportDao updateReportDao();

}
