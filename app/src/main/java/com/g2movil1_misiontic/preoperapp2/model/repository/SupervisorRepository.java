package com.g2movil1_misiontic.preoperapp2.model.repository;

import com.g2movil1_misiontic.preoperapp2.view.Supervisor.SupervisorMVP;
import com.google.firebase.auth.FirebaseAuth;

public class SupervisorRepository implements SupervisorMVP.Model {


    @Override
    public void logout() {
        FirebaseAuth.getInstance().signOut();
    }
}
