package com.g2movil1_misiontic.preoperapp2.model.repository;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;

import com.g2movil1_misiontic.preoperapp2.model.entity.User;
import com.g2movil1_misiontic.preoperapp2.view.Register.RegisterMVP;
import com.g2movil1_misiontic.preoperapp2.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.g2movil1_misiontic.preoperapp2.view.login.MainMVP;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public class UserRepository implements MainMVP.Model, RegisterMVP.Model {

    private static final String TAG = UserRepository.class.getSimpleName();
    private String rol;
    private FirebaseAuth mAuth;
    private MainMVP.Presenter mainPresenter;
    private RegisterMVP.Presenter registerpresenter;
    private FirebaseDatabase database = null;
    private DatabaseReference myRef = null;
    private DatabaseReference mDatabase;


/*
    public DatabaseReference getMyRef() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Users");

        //myRef.setValue("Hello, World!");
        return myRef;
    }

    /*public void prueba(){
        myRef.setValue("Hello, World!");
    }

    private List<User> users;*/

    public UserRepository() {
        mAuth = FirebaseAuth.getInstance();
        /*users = Arrays.asList(
                new User(1, "Jeick Ruiz","jruiz@email.com", "abc1", "supervisor", ""),
                new User(2, "Jorge","jorge@email.com", "abc2", "supervisor", ""),
                new User(3,"Leidy","leidy@email.com", "abc3", "supervisor", ""),
                new User(4, "Alex","alex@email.com", "abc4", "conductor", "SDA123"),
                new User(5, "Daniel","daniel@email.com", "abc5", "supervisor", "")

        );*/
    }

    @Override
    public void setMainPresenter(MainMVP.Presenter mainPresenter) {
        this.mainPresenter = mainPresenter;
    }

    @Override
    public void performGoogleLogin(Context context) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(context.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        GoogleSignInClient client = GoogleSignIn.getClient(context, gso);
        mainPresenter.setGoogleSingInClient(client);


    }

    @Override
    public void infoGoogleLogin(Intent data) {

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.d(TAG, "firebaseAuthWithGoogle:" + account.getId());
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
            }
    }


    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            mainPresenter.loginGoogleFinal();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                        }
                    }
                });
    }

    @Override
    public void validateUsernamePasswordSupervisor(String email, String password, Context context) {
       /* for (User user : users) {
            if (user.getEmail().equals(email)
                    && user.getPassword().equals(password)
                    && user.getRol().equals("supervisor")) {

                return true;
            }
        }*/

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            //el profe lo eliminó pero podria servirme

                           /* Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);*/

                            mainPresenter.authenticatedSucces();
                        } else {
                            // If sign in fails, display a message to the user.

                            //el profe lo eliminó pero podria servirme
                            /*
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(EmailPasswordActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);*/
                            mainPresenter.authenticatedFailure(task.getException().getMessage());
                        }
                    }
                });

        return;
    }

    @Override
    public boolean validateUsernamePasswordConductor(String email, String password) {
        /*for (User user : users) {
            if (user.getEmail().equals(email)
                    && user.getPassword().equals(password)
                    && user.getRol()=="conductor") {
                return true;
            }
        }*/
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            //el profe lo eliminó pero podria servirme

                            /*Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);*/

                            //mainPresenter.authenticatedSucces();

                            FirebaseUser currentUser = mAuth.getCurrentUser();
                            String id = currentUser.getUid();
                            if(currentUser != null){
                                myRef = FirebaseDatabase.getInstance().getReference();

                                myRef.child("Users").child(id).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if (snapshot.exists()){
                                            rol = snapshot.child("Rol").getValue().toString();
                                                if(rol.equals("Conductor")){
                                                    mainPresenter.authenticatedSuccesDriver();
                                                }else{
                                                    if(rol.equals("Supervisor")){
                                                    mainPresenter.authenticatedSucces();
                                                    }
                                                }


                                        }
                                    }


                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });



                        } else {
                            // If sign in fails, display a message to the user.

                            //el profe lo eliminó pero podria servirme
                            /*
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(EmailPasswordActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);*/
                            mainPresenter.authenticatedFailure(task.getException().getMessage());
                        }

                    };
        }
        });
        return true;

    }

    public boolean isAuthenticated(){
        //mDatabase = FirebaseDatabase.getInstance().getReference();
        /*String currentUser = mAuth.getCurrentUser().getUid();
        if(currentUser != null){


        mDatabase.child("Users").child(currentUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    rol = snapshot.child("Rol").getValue().toString();


                }
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });
        return rol;
        }*/

        //return "no";

        FirebaseUser currentUser = mAuth.getCurrentUser();
        return currentUser != null;
    }
/*
    @Override
    public void logOut() {
        FirebaseAuth.getInstance().signOut();
    }*/
}
