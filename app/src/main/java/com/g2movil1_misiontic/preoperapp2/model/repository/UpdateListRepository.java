package com.g2movil1_misiontic.preoperapp2.model.repository;

import com.g2movil1_misiontic.preoperapp2.view.UpdateList.UpdateListInfo;
import com.g2movil1_misiontic.preoperapp2.view.UpdateList.UpdateListMVP;

import java.util.Arrays;
import java.util.List;


public class UpdateListRepository implements UpdateListMVP.Model {

    @Override
    public List<UpdateListInfo> loadReport() {
        return Arrays.asList(
        new UpdateListInfo("QER 132", "Christian Nolan", "1"),
        new UpdateListInfo("QRS 560", "David Fernandez", "3"),
        new UpdateListInfo("ART 593", "Jorge Millar", "1"),
        new UpdateListInfo("PTE 249", "Esteban McQueen", "4"),
        new UpdateListInfo("TRR 530", "Damian Chaza", "1"),
        new UpdateListInfo("TUT 765", "Diego Rodriguez", "1"),
        new UpdateListInfo("UTR 943", "Daniel Villamizar", "6"),
        new UpdateListInfo("FSD 654", "Joaquin Quintero", "2")
        );
    }
}
