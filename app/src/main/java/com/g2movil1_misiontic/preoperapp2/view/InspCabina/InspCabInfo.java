package com.g2movil1_misiontic.preoperapp2.view.InspCabina;

public class InspCabInfo {
    private boolean nibsOK;
    private boolean nibsBad;
    private boolean whistleOK;
    private boolean whistleBad;
    private boolean alarmOK;
    private boolean alarmBad;
    private boolean auxwhistleOK;
    private boolean auxwhistleBad;
    private boolean beltOK;
    private boolean beltBad;
    private boolean lightingOK;
    private boolean lightingBad;
    private boolean airConditioningOK;
    private boolean airConditioningBad;
    private boolean indicatorsOK;
    private boolean indicatorsBad;

    public InspCabInfo(boolean nibsOK, boolean whistleOK, boolean alarmOK, boolean auxwhistleOK, boolean beltOK, boolean lightingOK, boolean airConditioningOK, boolean indicatorsOK,
                       boolean nibsBad, boolean whistleBad, boolean alarmBad, boolean auxwhistleBad, boolean beltBad, boolean lightingBad, boolean airConditioningBad, boolean indicatorsBad) {
        this.nibsOK = nibsOK;
        this.whistleOK = whistleOK;
        this.alarmOK = alarmOK;
        this.auxwhistleOK = auxwhistleOK;
        this.beltOK = beltOK;
        this.lightingOK = lightingOK;
        this.airConditioningOK = airConditioningOK;
        this.indicatorsOK = indicatorsOK;
        this.nibsBad = nibsBad;
        this.whistleBad = whistleBad;
        this.alarmBad = alarmBad;
        this.auxwhistleBad = auxwhistleBad;
        this.beltBad = beltBad;
        this.lightingBad = lightingBad;
        this.airConditioningBad = airConditioningBad;
        this.indicatorsBad = indicatorsBad;
    }

    public boolean getNibsOK() {
        return nibsOK;
    }

    public  boolean getWhistleOK(){
        return whistleOK;
    }

    public boolean getAlarmOK() {
        return alarmOK;
    }

    public boolean getAuxwhistleOK() {
        return auxwhistleOK;
    }

    public boolean getBeltOK() {
        return beltOK;
    }

    public boolean getLightingOK() {
        return lightingOK;
    }

    public boolean getAirConditioningOK() {
        return airConditioningOK;
    }

    public boolean getIndicatorsOK() {
        return indicatorsOK;
    }

    public boolean getNibsBad() {
        return nibsBad;
    }

    public  boolean getWhistleBad(){
        return whistleBad;
    }

    public boolean getAlarmBad() {
        return alarmBad;
    }

    public boolean getAuxwhistleBad() {
        return auxwhistleBad;
    }

    public boolean getBeltBad() {
        return beltBad;
    }

    public boolean getLightingBad() {
        return lightingBad;
    }

    public boolean getAirConditioningBad() {
        return airConditioningBad;
    }

    public boolean getIndicatorsBad() {
        return indicatorsBad;
    }

}
