package com.g2movil1_misiontic.preoperapp2.view.NewVehicle;

import java.util.Date;

public class InfoVehicle {
    private String marca;
    private String modelo;
    private String placa;
    private String poliza;
    private String soat;
    private String tecnicomecanica;

    public InfoVehicle(String marca, String modelo, String placa,String poliza, String soat, String tecnicomecanica) {
        this.marca = marca;
        this.modelo = modelo;
        this.placa = placa;
        this.poliza = poliza;
        this.soat = soat;
        this.tecnicomecanica = tecnicomecanica;
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public String getPoliza() {
        return poliza;
    }

    public String getSoat() {
        return soat;
    }

    public String getTecnicomecanica() {
        return tecnicomecanica;
    }
}
