package com.g2movil1_misiontic.preoperapp2.view.Update;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.g2movil1_misiontic.preoperapp2.view.Supervisor.HomeSupervisorActivity;
import com.g2movil1_misiontic.preoperapp2.R;


public class ViewUpdateActivity extends AppCompatActivity {

    Button btnReturn;
    TextView tvDriver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        btnReturn = findViewById(R.id.btn_return_driver);



        btnReturn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewUpdateActivity.this, HomeSupervisorActivity.class);

                startActivity(intent);
            }

            });

        getdata();
    }
//traer el nombre del conductor, la placa y la ubicación
    private void getdata() {
    }

}