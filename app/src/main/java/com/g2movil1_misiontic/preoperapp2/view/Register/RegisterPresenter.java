package com.g2movil1_misiontic.preoperapp2.view.Register;

import androidx.annotation.NonNull;

import com.g2movil1_misiontic.preoperapp2.view.InspCabina.FormInspCabinaActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class RegisterPresenter implements RegisterMVP.Presenter{
    private final RegisterMVP.View view;
    private final RegisterMVP.Model model;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private String name="";
    private String phone="";
    private String email="";
    private String password="";
    private String rol="";
    public RegisterPresenter(RegisterMVP.View view) {
        this.view = view;
        this.model = null;
    }


    @Override
    public void Validateregister() {
        Registerinfo info = view.getRegisterInfo();
        if (info.getUsername() == null || info.getUsername().trim().isEmpty()){
            view.ShowUsernameError("Nombre no válido");
            return;
        }
        if (info.getEmail() == null || info.getEmail().trim().isEmpty() || !info.getEmail().trim().contains("@")){
            view.ShowEmailError("E-mail no válido");
            return;
        }
        if (info.getPhone() == null || info.getPhone().trim().isEmpty()){
            view.ShowPhoneError("Teléfono no válido");
            return;
        }

        if (info.getRol() == null){
            view.ShowRolError("Rol no válido");
            return;
        }
        if (info.getPassword() == null || info.getPassword().trim().isEmpty()){
            view.ShowPasswordError("Contraseña no válida");
            return;
        }
        if (!info.getConfirmpassword().equals(info.getPassword()) || info.getConfirmpassword().trim().isEmpty()){
            view.ShowConfirmpasswordError("Contraseñas no coinciden");
            return;
        }

        NewRegister();



        //TODO faltaria hacer como se valida que el usuario ya existe en la bd
    }

    @Override


    public void NewRegister(){
        Registerinfo info = view.getRegisterInfo();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();


        name = info.getUsername();
        phone = info.getPhone();
        email = info.getEmail();
        password = info.getPassword();
        rol = info.getRol();

        if(password.length() >=6){
            registeruser();
        }else {
            view.ShowPasswordError("La contraseña debe tener minimo 6 caracteres");
        }

        view.ShowSuccess("El regístro se realizó con éxito");
    }
 private void registeruser(){
    mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            if(task.isSuccessful()){
                //creacion del mapa para enviar datos al fire
                Map<String, Object> data = new HashMap<>();
                data.put("Nombre", name);
                data.put("Telefono", phone);
                data.put("Email", email);
                data.put("Password", password);
                data.put("Rol", rol);

                String id = mAuth.getCurrentUser().getUid();
                mDatabase.child("Users").child(id).setValue(data).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task2) {
                      /* if(task2.isSuccessful()){
                            if(rol.equals("Administrador")){
                                startActivity(new Intent(com.g2movil1_misiontic.preoperapp2.view.login.Register.RegistrarseActivity.this, HomeSupervisorActivity.class));
                            }
                        }*/
                    }

                });
            }
        }
    });
 }

}
