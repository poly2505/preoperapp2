package com.g2movil1_misiontic.preoperapp2.view.UpdateList;

import com.g2movil1_misiontic.preoperapp2.model.repository.UpdateListRepository;
import com.g2movil1_misiontic.preoperapp2.view.Update.ViewUpdateActivity;

import java.util.List;

public class UpdateListPresenter implements UpdateListMVP.Presenter {
    private final UpdateListMVP.View view;
    private final UpdateListMVP.Model model;

    public UpdateListPresenter(UpdateListMVP.View view) {
        this.view = view;
        this.model = new UpdateListRepository();
    }


    @Override
    public void loadReport() {
        List<UpdateListInfo> updates = model.loadReport();
        view.loadReport(updates);
    }



    @Override
    public void newReport() {

        view.showActivity(ViewUpdateActivity.class);
    }
}
