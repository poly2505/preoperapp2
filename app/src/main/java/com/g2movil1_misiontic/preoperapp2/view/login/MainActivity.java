package com.g2movil1_misiontic.preoperapp2.view.login;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Instrumentation;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;

import com.g2movil1_misiontic.preoperapp2.view.Register.RegistrarseActivity;
import com.g2movil1_misiontic.preoperapp2.R;
import com.g2movil1_misiontic.preoperapp2.view.Update.ViewUpdateActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity implements MainMVP.View {

    private final int GOOGLE_LOGIN_RESULT = 1;
    private MainMVP.Presenter presenter;
    Context context;
    TextInputLayout tilUsername;
    TextInputEditText etUsername;
    TextInputLayout tilPassword;
    TextInputEditText etPassword;
    Button btnLogin;
    Button btnRegistrarse;
    Button btnGoogle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_main);

        btnRegistrarse = findViewById(R.id.btnRegistro);
        btnRegistrarse.setOnClickListener(v -> {

            Intent intent = new Intent(MainActivity.this, RegistrarseActivity.class);
            startActivity(intent);
        });

        initUI();

    }

    private void initUI() {
        presenter = new MainPresenter(this);

        tilUsername = findViewById(R.id.tEmail);
        etUsername = findViewById(R.id.eEmail);
        tilPassword = findViewById(R.id.tpassword);
        etPassword = findViewById(R.id.epassword);

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> {
            tilUsername.setError("");
            tilPassword.setError("");
            // Presentador llama el metodo iniciarSesion()
            presenter.login();
            // Modificaciones posible arreglo rol


        });
        btnGoogle = findViewById(R.id.btn_loggmail);
        btnGoogle.setOnClickListener(v -> {
            tilUsername.setError("");
            tilPassword.setError("");
            // Presentador llama el metodo iniciarSesionGoogle()
            presenter.loginGoogle(getApplicationContext());
        });
    }
/*
    @Override
    public void onBackPressed() {
        presenter.LogOut();
    }*/

    @Override
    protected void onStart() {
        super.onStart();

        presenter.authenticated();
    }

    @Override
    public LoginInfo getLoginInfo() {
        return new LoginInfo(
                etUsername.getText().toString().trim(),
                etPassword.getText().toString().trim());
    }

    @Override
    public void showEmailError(String message) {

        tilUsername.setError(message);
    }

    @Override
    public void showPasswordError(String message) {

        tilPassword.setError(message);
    }

    @Override
    public void showErrorMessage(String message) {
        Snackbar.make(btnLogin, message, Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void showGooglelogin(Intent signInIntent) {
        googleLauncher.launch(signInIntent);
    }
    /*
    @Override
    public void confirmLogOut() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage("¿Está seguro de cerrar sesión?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        presenter.logOutConfirmed();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();


    }*/


    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, type);
        startActivity(intent);
    }

    ActivityResultLauncher<Intent> googleLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode()== Activity.RESULT_OK){
                    Intent data = result.getData();
                    presenter.googleLoginInfo(data);
                }
            });
    public void showActivityGoogle(Intent intent){
        googleLauncher.launch(intent);
    }

}
