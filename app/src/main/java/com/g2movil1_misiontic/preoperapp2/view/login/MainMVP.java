package com.g2movil1_misiontic.preoperapp2.view.login;
import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;

public interface MainMVP  {
    interface Model {

        void validateUsernamePasswordSupervisor(String username, String password, Context context);

        boolean validateUsernamePasswordConductor(String username, String password);

        void setMainPresenter(Presenter presenter);

        void performGoogleLogin(Context context);

        void infoGoogleLogin(Intent data);

        boolean isAuthenticated();

        //void logOut();
    }

    interface Presenter {

        void login();

        void loginGoogle(Context context);

        void googleLoginInfo(Intent data);

        void setGoogleSingInClient(GoogleSignInClient client);

        void loginGoogleFinal();

        void authenticated();

        void  authenticatedSucces();

        void authenticatedFailure(String message);

        void  authenticatedSuccesDriver();

        //void LogOut();

        //void logOutConfirmed();
    }

    interface View {
        LoginInfo getLoginInfo();

        void showEmailError(String message);

        void showPasswordError(String message);

        void showActivity(Class<? extends AppCompatActivity> type);

        void showErrorMessage(String message);

        void showGooglelogin(Intent signInIntent);

        //void confirmLogOut();
    }

}
