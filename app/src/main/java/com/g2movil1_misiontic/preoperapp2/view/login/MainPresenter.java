package com.g2movil1_misiontic.preoperapp2.view.login;
import android.content.Context;
import android.content.Intent;

import com.g2movil1_misiontic.preoperapp2.view.Supervisor.HomeSupervisorActivity;
import com.g2movil1_misiontic.preoperapp2.model.repository.UserRepository;
import com.g2movil1_misiontic.preoperapp2.view.HomeConductor.HomeConductorActivity;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;

public class MainPresenter implements MainMVP.Presenter  {
    private final MainMVP.View view;
    private final MainMVP.Model model;

    public MainPresenter(MainMVP.View view) {
        this.view = view;
        this.model = new UserRepository();
        this.model.setMainPresenter(this);
    }

    @Override
    public void login() {
        LoginInfo info = view.getLoginInfo();

        // Validar Datos
        if (info.getEmail() == null
                || info.getEmail().trim().isEmpty()
                || !info.getEmail().contains("@")) {
            view.showEmailError("Correo electronico no válido");
            return;
        }

        if (info.getPassword() == null
                || info.getPassword().trim().isEmpty()) {
            view.showPasswordError("Contraseña no válida");
            return;
        }
       // model.validateUsernamePasswordSupervisor(info.getEmail(), info.getPassword(), null);
        //validar posible error
        model.validateUsernamePasswordConductor(info.getEmail(), info.getPassword());

        /* if(){


        } else {
            if(){

            } else {

            }


        }*/


    }


    @Override
    public void loginGoogle(Context context) {
        model.performGoogleLogin(context);
    }

    @Override
    public void googleLoginInfo(Intent data) {
        model.infoGoogleLogin(data);
    }

    @Override
    public void setGoogleSingInClient(GoogleSignInClient client) {
        view.showGooglelogin(client.getSignInIntent());
    }

    @Override
    public void loginGoogleFinal() {
        view.showActivity(HomeConductorActivity.class);
    }

    @Override
    public void authenticated() {
        /*if(model.isAuthenticated().equals("Supervisor")){

            view.showActivity(HomeSupervisorActivity.class);
        }else{
            if(model.isAuthenticated().equals("Conductor")){
                view.showActivity(HomeConductorActivity.class);
            }else {
                if(model.isAuthenticated().equals("no")){
                    view.showActivity(MainActivity.class);
                }
            }
        }*/

        if(model.isAuthenticated()){
            view.showActivity(HomeSupervisorActivity.class);
        }

    }

    @Override
    public void authenticatedSucces() {
        view.showActivity(HomeSupervisorActivity.class);
    }

    @Override
    public void authenticatedFailure(String message) {
        view.showErrorMessage( message /*"Usuario y contraseña no coinciden"*/);
    }

    @Override
    public void authenticatedSuccesDriver() {
        view.showActivity(HomeConductorActivity.class);
    }

    /*@Override
    public void LogOut() {
        view.confirmLogOut();
    }*/
/*
    @Override
    public void logOutConfirmed() {
        model.logOut();
        view.showActivity(MainActivity.class);
    }*/
}
