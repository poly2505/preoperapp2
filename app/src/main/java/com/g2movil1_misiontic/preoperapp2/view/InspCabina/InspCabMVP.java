package com.g2movil1_misiontic.preoperapp2.view.InspCabina;

import androidx.appcompat.app.AppCompatActivity;

import com.g2movil1_misiontic.preoperapp2.R;

public interface InspCabMVP {
    interface Model {

    }
    interface Presenter {

        void validateInspection();
        void backHome();

        void saveDataGeo(String Date, String Hour, String Placa);
    }
    interface  View {
        InspCabInfo getInspCabInfo();
        void ShowNibError(String message);
        void ShowWhistleError(String message);
        void ShowAlarmError(String message);
        void ShowAuxWhistleError(String message);
        void ShowBeltError(String message);
        void ShowLightingError(String message);
        void ShowAirConditioningError(String message);
        void ShowIndicatorsError(String message);
        void ShowSuccess(String message);
        void showActivity(Class<? extends AppCompatActivity> type);
        void successDialog();

    }
}
