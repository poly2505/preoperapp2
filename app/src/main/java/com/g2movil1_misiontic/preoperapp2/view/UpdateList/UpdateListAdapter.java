package com.g2movil1_misiontic.preoperapp2.view.UpdateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.g2movil1_misiontic.preoperapp2.R;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;

public class UpdateListAdapter extends RecyclerView.Adapter<UpdateListAdapter.ViewHolder> {

   private final List<UpdateListInfo> data;


    public UpdateListAdapter(List<UpdateListInfo> data) {
       this.data = data;

    }

    @NonNull

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_placas, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull UpdateListAdapter.ViewHolder holder, int position) {
        UpdateListInfo list = data.get(position);
        holder.getTxtPlacasName().setText(list.getPlaca());
        holder.getTxtNumberReport().setText(list.getNumberReport());
        holder.getTxtDriverName().setText(list.getDriver());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final MaterialTextView txtPlacasName;
        private final MaterialTextView txtDriverName;
        private final MaterialTextView txtNumberReport;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtDriverName = itemView.findViewById(R.id.txt_driver_name);
            txtPlacasName = itemView.findViewById(R.id.txt_placas_name);
            txtNumberReport = itemView.findViewById(R.id.txt_number_Report);

        }

        public MaterialTextView getTxtPlacasName() {
            return txtPlacasName;
        }

        public MaterialTextView getTxtDriverName() {
            return txtDriverName;
        }

        public MaterialTextView getTxtNumberReport() {
            return txtNumberReport;
        }
    }
}