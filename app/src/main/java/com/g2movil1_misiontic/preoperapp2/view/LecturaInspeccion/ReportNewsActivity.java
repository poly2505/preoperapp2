package com.g2movil1_misiontic.preoperapp2.view.LecturaInspeccion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.widget.TextView;

import com.g2movil1_misiontic.preoperapp2.R;
import com.g2movil1_misiontic.preoperapp2.model.entity.Inspecciones;
import com.g2movil1_misiontic.preoperapp2.model.repository.ChargeInfo;
import com.g2movil1_misiontic.preoperapp2.view.Supervisor.HomeSupervisorActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ReportNewsActivity extends AppCompatActivity implements ReportNewActivityMVP.View {

    private ReportNewActivityMVP.Presenter presenter;

    Button btnReturn;
    String placaNow;
    String fechaNow;
    String horaNow;
    String air;
    String alarm;
    String belt;
    String hora;
    String light;
    String indications;
    String whistle;
    String  wistleAux;
    String nibs;
    TextView Placa;
    TextView Air;
    TextView Alarm;
    TextView Belt;
    TextView Hora;
    TextView Light;
    TextView Indications;
    TextView Whistle;
    TextView WistleAux;
    TextView Nibs;

    private DatabaseReference myRef = null;
    private static final String TAG = "ReportNewActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_news);
        loadPlaca();
        btnReturn = findViewById(R.id.btn_return_driver);
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //loadDatos();
                Intent intent = new Intent(ReportNewsActivity.this, HomeSupervisorActivity.class);
                startActivity(intent);
            }
        });
    }



    @Override
    public void loadPlaca() {
        Bundle dataplaca = getIntent().getExtras();
        placaNow = dataplaca.getString("placaB");
        fechaNow = dataplaca.getString("fechaB");
        horaNow = dataplaca.getString("horaB");
        Log.d(TAG,"Fecha revision : "+horaNow);
        loadDatos();
        //presenter.savePlaca(placaNow);
    }

    private void loadDatos() {
                 Placa = findViewById(R.id.tv_infovehicle);
                 Air = findViewById(R.id.Aire);
                    Alarm = findViewById(R.id.Alarma);
                    Belt = findViewById(R.id.cinturon);
                    Hora = findViewById(R.id.Hora);
                    Light = findViewById(R.id.Iluminacion);
                    Indications = findViewById(R.id.Indicador);
                    Whistle = findViewById(R.id.Pito);
                    WistleAux = findViewById(R.id.PitoAux);
                    Nibs = findViewById(R.id.Plumillas);
                 Placa.setText(placaNow);

        myRef = FirebaseDatabase.getInstance().getReference();

        myRef.child("Inspecciones").child(placaNow).child(fechaNow+"_"+horaNow).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    air = snapshot.child("Aire_Acondicionado").getValue().toString();
                    Air.setText("Aire Aconicionado: "+air);
                    alarm = snapshot.child("Alarma_de_testigo").getValue().toString();
                    Alarm.setText("Alarma de testigo: "+alarm);
                    belt = snapshot.child("Cinturon_de_seguridad").getValue().toString();
                    Belt.setText("Cinturon de seguridad: "+belt);
                    hora = snapshot.child("Hora").getValue().toString();
                    Hora.setText("Hora de inspección: "+hora);
                    light = snapshot.child("Iluminacion_de_cabina").getValue().toString();
                    Light.setText("Iluminación de cabina: "+light);
                    indications = snapshot.child("Indicadores_luces_direccionales").getValue().toString();
                    Indications.setText("Indicadores de luces direccionales: "+indications);
                    whistle = snapshot.child("Pito_Electrico").getValue().toString();
                    Whistle.setText("Pito eléctrico: "+whistle);
                    wistleAux = snapshot.child("Pito_Auxiliar").getValue().toString();
                    WistleAux.setText("Pito auxiliar: "+wistleAux);
                    nibs = snapshot.child("Plumillas_limpiavidrios").getValue().toString();
                    Nibs.setText("Plumillas limpavidrios: "+nibs);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }
}