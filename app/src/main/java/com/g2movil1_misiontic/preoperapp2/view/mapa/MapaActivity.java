package com.g2movil1_misiontic.preoperapp2.view.mapa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.location.Location;
import android.os.Bundle;

import com.g2movil1_misiontic.preoperapp2.R;
import com.g2movil1_misiontic.preoperapp2.model.repository.LocationRepository;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jetbrains.annotations.NotNull;

public class MapaActivity extends AppCompatActivity implements OnMapReadyCallback {
    LocationRepository repository;
    LatLng latLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        repository = new LocationRepository(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geolocalizacion_conductor);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        // TODO ¿Qué hacer con el mapa? Guardar la ubicación del conductor
        // googleMap.setMyLocationEnabled(true);
        //googleMap.isMyLocationEnabled();
        googleMap.getUiSettings().setZoomControlsEnabled(true);

    repository.addLocationUpdateCallback(new LocationCallback() {

        @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location loc = repository.getCurrentLocation();
                LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());

                googleMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("Aqui estoy!"));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            }
        });

    }
}
