package com.g2movil1_misiontic.preoperapp2.view.ViewReport;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

import com.g2movil1_misiontic.preoperapp2.R;
import com.g2movil1_misiontic.preoperapp2.view.LecturaInspeccion.ReportNewsActivity;
import com.g2movil1_misiontic.preoperapp2.model.repository.ChargeInfo;
import com.g2movil1_misiontic.preoperapp2.view.ReportSearch.ReportActivity;

import java.util.List;

public class ViewReportActivity extends AppCompatActivity implements ViewReportMVP.View {

    private ViewReportMVP.Presenter presenter;

    Button btnBackReport;
    RecyclerView rvCharges;
    private String placaNow;
    private static final String TAG="ViewReportActivity";
    //Button btnReportNews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_report);

        initUI();
        dataPlaca();


        btnBackReport = findViewById(R.id.btn_BackReport);
        btnBackReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewReportActivity.this, ReportActivity.class);
                startActivity(intent);
            }
        });
        /*btnReportNews = findViewById(R.id.btn_report_news);
        btnReportNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewReportActivity.this, ViewUpdateActivity.class);
                startActivity(intent);
            }
        });*/
    }

    private void initUI() {
        presenter = new ViewReportPresenter(this);

        rvCharges = findViewById(R.id.rv_charges);
        rvCharges.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.loadData();
    }

    @Override
    public void dataPlaca() {
        Bundle dataplaca = getIntent().getExtras();
        placaNow = dataplaca.getString("placaB");

        presenter.savePlaca(placaNow);
    }

    @Override
    public void loadCharges(List<ChargeInfo> charges) {

        ViewReportAdapter adapter = new ViewReportAdapter(charges);

        adapter.setOnItemClickListener(info -> {
            Intent intent = new Intent(ViewReportActivity.this, ReportNewsActivity.class);
            intent.putExtra("placaB", placaNow);
            intent.putExtra("fechaB", info.getFecha());
            intent.putExtra("horaB",info.getHora());
            //intent.putExtra("Fecha",charges)
            startActivity(intent);

            //startActivity(intent);

            /*
            Uri gmmIntentUri = Uri.parse("geo:0,0?q="+info.getAddress());
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
            */

            //TODO presenter.openPay(info);
        });


        rvCharges.setAdapter(adapter);
    }
}