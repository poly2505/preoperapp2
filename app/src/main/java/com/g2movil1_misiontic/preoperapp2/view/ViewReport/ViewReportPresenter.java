package com.g2movil1_misiontic.preoperapp2.view.ViewReport;

import com.g2movil1_misiontic.preoperapp2.model.repository.ChargeInfo;
import com.g2movil1_misiontic.preoperapp2.model.repository.ViewReportRepository;

import java.util.List;

public class ViewReportPresenter implements ViewReportMVP.Presenter {

    private final ViewReportMVP.View view;
    private final ViewReportMVP.Model model;

    public ViewReportPresenter(ViewReportMVP.View view) {
        this.view = view;
        this.model = new ViewReportRepository(view.getApplicationContext(), this);
    }

    @Override
    public void loadData() {
        model.loadData();
    }

    @Override
    public void loadCharges(List<ChargeInfo> charges) {
        view.loadCharges(charges);
    }

    @Override
    public void savePlaca(String placaNow) {
        model.savePlaca(placaNow);
    }
}
