package com.g2movil1_misiontic.preoperapp2.view.Register;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

import com.g2movil1_misiontic.preoperapp2.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class RegistrarseActivity extends AppCompatActivity implements RegisterMVP.View {

    AppCompatAutoCompleteTextView spRol;

    private RegisterMVP.Presenter presenter;
    Button btnRegister;
    TextInputLayout tilUsername;
    TextInputEditText etUsername;
    TextInputLayout tilPhone;
    TextInputEditText etPhone;
    TextInputLayout tilEmail;
    TextInputEditText etEmail;
    TextInputLayout tilRol;
    AppCompatAutoCompleteTextView etRol;
    TextInputLayout tilPassword;
    TextInputEditText etPassword;
    TextInputLayout tilConfirmpassword;
    TextInputEditText etConfirmpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);
        
        initUI();

        presenter = new RegisterPresenter(this);
        tilUsername = findViewById(R.id.tname);
        etUsername = findViewById(R.id.ename);
        tilPhone = findViewById(R.id.tnumtel);
        etPhone = findViewById(R.id.enumtel);
        tilEmail = findViewById(R.id.tEmail);
        etEmail = findViewById(R.id.eEmail);
        tilRol = findViewById(R.id.trol);
        etRol = findViewById(R.id.erol);
        tilPassword = findViewById(R.id.tpassword);
        etPassword = findViewById(R.id.epassword);
        tilConfirmpassword = findViewById(R.id.tconfpassword);
        etConfirmpassword = findViewById(R.id.econfpassword);
        btnRegister = findViewById(R.id.btnRegistro2);
        btnRegister.setOnClickListener(v -> {
            tilUsername.setError("");
            tilPhone.setError("");
            tilEmail.setError("");
            tilRol.setError("");
            tilPassword.setError("");
            tilConfirmpassword.setError("");
            presenter.Validateregister();
            //presenter.NewRegister();
        });
    }

    private void initUI() {
        spRol = findViewById(R.id.erol);
        String[] roles = getResources().getStringArray(R.array.roles);
        spRol.setAdapter(new ArrayAdapter<>(RegistrarseActivity.this, android.R.layout.preference_category, roles));
    }

    @Override
    public Registerinfo getRegisterInfo() {

        return new Registerinfo(etUsername.getText().toString().trim(),
                                etPhone.getText().toString().trim(),
                                etEmail.getText().toString().trim(),
                                etRol.getText().toString().trim(),
                                etPassword.getText().toString().trim(),
                                etConfirmpassword.getText().toString().trim());
    }

    @Override
    public void ShowUsernameError(String message) {
        tilUsername.setError(message);
    }

    @Override
    public void ShowPhoneError(String message) {
        tilPhone.setError(message);
    }

    @Override
    public void ShowEmailError(String message) {
        tilEmail.setError(message);
    }

    @Override
    public void ShowPasswordError(String message) { tilPassword.setError(message); }

    @Override
    public void ShowConfirmpasswordError(String message) {
        tilConfirmpassword.setError(message);
    }

    @Override
    public void ShowRolError(String message) {
        tilRol.setError(message);
    }

    @Override
    public void ShowSuccess(String message) {

    }
}