package com.g2movil1_misiontic.preoperapp2.view.LecturaInspeccion;

import android.content.Context;

public interface ReportNewActivityMVP {
    interface Model{
        
    }
    
    interface View{
        void loadPlaca();

        Context getApplicationContext();
    }
    interface Presenter{
        void savePlaca(String placaNow);
    }
}
