package com.g2movil1_misiontic.preoperapp2.view.HomeConductor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.g2movil1_misiontic.preoperapp2.PerfilActivity;
import com.g2movil1_misiontic.preoperapp2.R;
import com.g2movil1_misiontic.preoperapp2.view.Geolocalizacion.GeolocalizacionActivity;
import com.g2movil1_misiontic.preoperapp2.view.InspCabina.FormInspCabinaActivity;


public class HomeConductorActivity extends AppCompatActivity {

    Button btn_perfilC;
    Button btnReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_condutor);

        btn_perfilC = findViewById(R.id.btn_PerfilCond);
        btn_perfilC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeConductorActivity.this, PerfilActivity.class);
                startActivity(intent);
            }
        });

        btnReport = findViewById(R.id.btn_Inspeccion);
        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeConductorActivity.this, GeolocalizacionActivity.class);
                startActivity(intent);

            }
        });

    }
}
