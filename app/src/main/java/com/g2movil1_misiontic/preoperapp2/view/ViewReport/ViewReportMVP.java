package com.g2movil1_misiontic.preoperapp2.view.ViewReport;

import android.content.Context;

import com.g2movil1_misiontic.preoperapp2.model.repository.ChargeInfo;

import java.util.List;

public interface ViewReportMVP {

    interface View{

        void dataPlaca();

        Context getApplicationContext();

        void loadCharges(List<ChargeInfo> charges);
    }

    interface  Model{

        void loadData();

        void savePlaca(String placaNow);
    }

    interface Presenter{

        void loadData();

        void loadCharges(List<ChargeInfo> charges);

        void savePlaca(String placaNow);
    }

}
