package com.g2movil1_misiontic.preoperapp2.view.NewVehicle;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.g2movil1_misiontic.preoperapp2.BuscarClaseVehiculoActivity;
import com.g2movil1_misiontic.preoperapp2.BuscarConductorActivity;
import com.g2movil1_misiontic.preoperapp2.R;
import com.g2movil1_misiontic.preoperapp2.view.Register.Registerinfo;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class CrearVehiculoActivity extends AppCompatActivity implements NewVehicleMVP.View {

    private NewVehicleMVP.Presenter presenter;
    Button btnbuscarCond;
    Button btnbuscarClaVehi;
    Button btnregistrar;
    TextInputLayout tilMarcaVehiculo;
    TextInputEditText etMarcaVehiculo;
    TextInputLayout tilModelo;
    TextInputEditText etModelo;
    TextInputLayout tilPlaca;
    TextInputEditText etPlaca;
    TextInputLayout tilPoliza;
    TextInputEditText etPoliza;
    EditText etFechaSOAT;
    EditText etFechaTecMecanica;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_vehiculo);

        iniUI();


        btnbuscarCond= findViewById(R.id.btn_conductor);
        btnbuscarCond.setOnClickListener(v -> {
            Intent intent = new Intent(CrearVehiculoActivity.this, BuscarConductorActivity.class);
            startActivity(intent);
        });

        btnbuscarClaVehi= findViewById(R.id.btn_clVehi);
        btnbuscarClaVehi.setOnClickListener( v ->{
            Intent intent   =  new Intent(CrearVehiculoActivity.this, BuscarClaseVehiculoActivity.class);
            startActivity(intent);
        });

        btnregistrar= findViewById(R.id.btn_guardar);
        btnregistrar.setOnClickListener( v ->{
            tilMarcaVehiculo.setError("");
            tilModelo.setError("");
            tilPlaca.setError("");
            tilPoliza.setError("");
            presenter.ValidateNewregister();
        });
    }

    private void iniUI() {
        presenter = new NewVehiclePresenter(this);
        tilMarcaVehiculo = findViewById(R.id.tMarca);
        etMarcaVehiculo = findViewById(R.id.eMarca);
        tilModelo = findViewById(R.id.tmodelo);
        etModelo = findViewById(R.id.emodelo);
        tilPlaca = findViewById(R.id.tPlaca);
        etPlaca = findViewById(R.id.ePlaca);
        tilPoliza = findViewById(R.id.tpoliza);
        etPoliza = findViewById(R.id.epoliza);
        etFechaSOAT = findViewById(R.id.etfsoat);
        etFechaTecMecanica = findViewById(R.id.etftecnico);

    }

    public InfoVehicle getRegisterVehicle(){
        return new InfoVehicle(
                etMarcaVehiculo.getText().toString().trim(),
                etModelo.getText().toString().trim(),
                etPlaca.getText().toString().trim(),
                etPoliza.getText().toString().trim(),
                etFechaSOAT.getText().toString().trim(),
                etFechaSOAT.getText().toString().trim());
    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, type);
        startActivity(intent);
    }

    @Override
    public void ShowMarcaError(String message) {
        tilMarcaVehiculo.setError(message);
    }


    @Override
    public void ShowModeloError(String message) {
        tilModelo.setError(message);
    }

    @Override
    public void ShowPlacaError(String message) {
        tilPlaca.setError(message);
    }


    @Override
    public void ShowPolizaError(String message) {
        tilPoliza.setError(message);
    }

    @Override
    public void ShowFechaSoatError(String message) {

    }

    @Override
    public void ShowfechaTecinomecanicaError(String message) {

    }

    @Override
    public void ShowRegisterSuccess(String message) {

    }

    @Override
    public void successDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage("El vehículo fue creado correctamente")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        presenter.backHome();
                    }
                })
                .show();
    }

}