package com.g2movil1_misiontic.preoperapp2.view.UpdateList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.g2movil1_misiontic.preoperapp2.R;
import com.g2movil1_misiontic.preoperapp2.view.Update.ViewUpdateActivity;

import java.util.ArrayList;
import java.util.List;


public class UpdateListActivity extends AppCompatActivity implements UpdateListMVP.View {

    private UpdateListMVP.Presenter presenter;
    private RecyclerView rvPlacas;

    Button btnGo;

    /*private ArrayList<UpdateListInfo> placas;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_list);

        btnGo = findViewById(R.id.btn_go);
       btnGo.setOnClickListener(this::onGoClick);
        //Llamo el método
        initUI();

        presenter.loadReport();
        /*generateData();
        setData();*/

    }

    private void initUI() {
        presenter = new UpdateListPresenter(this);
        rvPlacas = findViewById(R.id.rv_placas);



    }


    @Override
    public void loadReport(List<UpdateListInfo> updates) {
        rvPlacas.setLayoutManager(new LinearLayoutManager(this));
        rvPlacas.setAdapter(new UpdateListAdapter(updates));
    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, ViewUpdateActivity.class);
        startActivity(intent);
    }

    private void onGoClick(View view) {
        //Abrir la activity ViewReport / Open the Activity ViexReport
        Intent intent = new Intent(this, ViewUpdateActivity.class);
        startActivity(intent);

    }


}

