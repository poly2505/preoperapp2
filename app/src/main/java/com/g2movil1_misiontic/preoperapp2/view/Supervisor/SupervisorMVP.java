package com.g2movil1_misiontic.preoperapp2.view.Supervisor;

import androidx.appcompat.app.AppCompatActivity;

import com.g2movil1_misiontic.preoperapp2.view.login.MainActivity;

public interface SupervisorMVP {
    interface Model {


        void logout();
    }

    interface Presenter {


        void LogOut();

        void logOutConfirmed();
    }

    interface View {

        void confirmLogOut();

        void showActivity(Class<? extends AppCompatActivity> type);
    }
}
