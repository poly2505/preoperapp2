package com.g2movil1_misiontic.preoperapp2.view.Register;

public class Registerinfo {
    private String username;
    private String phone;
    private String email;
    private String rol;
    private String password;
    private String confirmpassword;

    public Registerinfo(String username, String phone, String email,String rol, String password, String confirmpassword) {
        this.username = username;
        this.phone = phone;
        this.email = email;
        this.rol = rol;
        this.password = password;
        this.confirmpassword = confirmpassword;
    }

    public String getUsername() {
        return username;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getRol() {
        return rol;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmpassword() {
        return confirmpassword;
    }
}
