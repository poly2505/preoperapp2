package com.g2movil1_misiontic.preoperapp2.view.Geolocalizacion;

public class GeolocalizacionPresenter implements  GeolocalizacionMVP.Presenter{

    private GeolocalizacionMVP.View view;
    //private GeolocalizacionMVP.Model model;


    public  GeolocalizacionPresenter (GeolocalizacionMVP.View view){
        this.view = view;
        // this.model = new GeolocalizacionMVP();
        this.view.getLocationPermision();
    }

}
