package com.g2movil1_misiontic.preoperapp2.view.LecturaInspeccion;

import com.g2movil1_misiontic.preoperapp2.model.repository.ViewReportRepository;
import com.g2movil1_misiontic.preoperapp2.view.ViewReport.ViewReportPresenter;

public class ReportNewPresenter implements ReportNewActivityMVP.Presenter{
    private final ReportNewActivityMVP.View view;
    private final ReportNewActivityMVP.Model model;
    private String placa;


    public ReportNewPresenter(ReportNewActivityMVP.View view) {
        this.view = view;
        this.model = null;

    }

    @Override
    public void savePlaca(String placaNow) {
        placa = placaNow;

    }
}
