package com.g2movil1_misiontic.preoperapp2.view.InspCabina;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.g2movil1_misiontic.preoperapp2.view.HomeConductor.HomeConductorActivity;
import com.g2movil1_misiontic.preoperapp2.R;

public class FormInspCabinaActivity extends AppCompatActivity implements InspCabMVP.View {

        private InspCabMVP.Presenter presenter;
        private Button btn_Rodamiento;
        private Button btnSaveInsp;
        private String dateNow;
        private String hourNow;
        private String placaNow;
        private RadioButton rbNibOk;
        private RadioButton rbNibBad;
        private RadioButton rbWhistleOk;
        private RadioButton rbWhistleBad;
        private RadioButton rbAlarmOk;
        private RadioButton rbAlarmBad;
        private RadioButton rbAuxWhistleOK;
        private RadioButton rbAuxWhistleBad;
        private RadioButton rbBeltOK;
        private RadioButton rbBeltBad;
        private RadioButton rbLightingOK;
        private RadioButton rbLightingBad;
        private  RadioButton rbAirConditioningOk;
        private  RadioButton rbAirConditioningBad;
        private  RadioButton rbIndicatorsOK;
        private  RadioButton rbIndicatorsBad;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_form_insp_cabina);

            presenter = new InspCabPresenter(this);
            rbNibOk = findViewById(R.id.rb_nibOk);
            rbNibBad = findViewById(R.id.rb_nibbad);
            rbWhistleOk = findViewById(R.id.rb_whistleOk);
            rbWhistleBad = findViewById(R.id.rb_auxWhistleBad);
            rbAlarmOk = findViewById(R.id.rb_alarmOk);
            rbAlarmBad = findViewById(R.id.rb_alarmbad);
            rbAuxWhistleOK = findViewById(R.id.rb_auxWhistleOk);
            rbAuxWhistleBad = findViewById(R.id.rb_auxWhistleBad);
            rbBeltOK = findViewById(R.id.rb_beltOK);
            rbBeltBad = findViewById(R.id.rb_beltBad);
            rbLightingOK = findViewById(R.id.rb_lightingOk);
            rbLightingBad = findViewById(R.id.rb_lightingBad);
            rbAirConditioningOk = findViewById(R.id.rb_airConditioningOk);
            rbAirConditioningBad = findViewById(R.id.rb_airConditioningBad);
            rbIndicatorsOK = findViewById(R.id.rb_indicatorsOk);
            rbIndicatorsBad = findViewById(R.id.rb_indicatorsBad);


            dataGeolocalitation();




            btn_Rodamiento = findViewById(R.id.btn_InspRod);
            btn_Rodamiento.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FormInspCabinaActivity.this, HomeConductorActivity.class);
                    startActivity(intent);
                }
            });

            btnSaveInsp = findViewById(R.id.btn_saveInspection);
            btnSaveInsp.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    presenter.validateInspection();
                }
            });


        }

        public InspCabInfo getInspCabInfo(){



            return new InspCabInfo(rbNibOk.isChecked(),rbWhistleOk.isChecked(),rbAlarmOk.isChecked(),rbAuxWhistleOK.isChecked(),rbBeltOK.isChecked(),
                    rbLightingOK.isChecked(),rbAirConditioningOk.isChecked(),rbIndicatorsOK.isChecked(),rbNibBad.isChecked(),rbWhistleBad.isChecked(),
                    rbAlarmBad.isChecked(),rbAuxWhistleBad.isChecked(),rbBeltBad.isChecked(), rbLightingBad.isChecked(),rbAirConditioningBad.isChecked(),
                    rbIndicatorsBad.isChecked());
        }

        //TODO ubicación del error y que clase de error se valída

    @Override
    public void ShowNibError(String message) {

    }

    @Override
    public void ShowWhistleError(String message) {

    }

    @Override
    public void ShowAlarmError(String message) {

    }

    @Override
    public void ShowAuxWhistleError(String message) {

    }

    @Override
    public void ShowBeltError(String message) {

    }

    @Override
    public void ShowLightingError(String message) {

    }

    @Override
    public void ShowAirConditioningError(String message) {

    }

    @Override
    public void ShowIndicatorsError(String message) {

    }

    @Override
    public void ShowSuccess(String message) {

    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, type);
        startActivity(intent);
    }

    @Override
    public void successDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage("La inspección se realizó con éxito")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        presenter.backHome();
                    }
                })
                .show();
    }

    public void dataGeolocalitation (){
            Bundle dataGeo = getIntent().getExtras();
            dateNow = dataGeo.getString("Fecha");
            hourNow = dataGeo.getString("Hora");
            placaNow = dataGeo.getString("Placa");

            presenter.saveDataGeo(dateNow, hourNow, placaNow);

        }

}