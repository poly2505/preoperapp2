package com.g2movil1_misiontic.preoperapp2.view.NewVehicle;

import androidx.appcompat.app.AppCompatActivity;

public interface NewVehicleMVP {
    interface Model{

    }
    interface Presenter{
        void ValidateNewregister();

        void backHome();
    }
    interface View{
        InfoVehicle getRegisterVehicle();
        void showActivity(Class<? extends AppCompatActivity> type);
        void ShowMarcaError(String message);
        void ShowPlacaError(String message);
        void ShowModeloError(String message);
        void ShowPolizaError(String message);
        void ShowFechaSoatError(String message);
        void  ShowfechaTecinomecanicaError(String message);
        void ShowRegisterSuccess(String message);
        void successDialog();
    }
}
