package com.g2movil1_misiontic.preoperapp2.view.Register;

public interface RegisterMVP {

    interface Model {

    }
    interface Presenter{
        void Validateregister();
        void NewRegister();
    }

    interface View{
        Registerinfo getRegisterInfo();

        void ShowUsernameError(String message);
        void ShowEmailError(String message);
        void ShowPhoneError(String message);
        void ShowPasswordError(String message);
        void ShowConfirmpasswordError(String message);
        void  ShowRolError(String message);
        void ShowSuccess(String message);
    }


}
