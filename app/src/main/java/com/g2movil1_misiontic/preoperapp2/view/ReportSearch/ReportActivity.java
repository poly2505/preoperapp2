package com.g2movil1_misiontic.preoperapp2.view.ReportSearch;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

import com.g2movil1_misiontic.preoperapp2.R;
import com.g2movil1_misiontic.preoperapp2.view.ViewReport.ViewReportActivity;
import com.google.android.material.textfield.TextInputEditText;


public class ReportActivity extends AppCompatActivity implements ReportMVP.View {


    private ReportMVP.Presenter presenter;
    private Button btnReportView;
    TextInputEditText PlacaBu;
    private String placa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        btnReportView = findViewById(R.id.btn_view_report);
        btnReportView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacaBu = findViewById(R.id.PlacaB);
                placa = PlacaBu.getText().toString().trim();

                Intent intent = new Intent(ReportActivity.this, ViewReportActivity.class);
                intent.putExtra("placaB", placa);
                startActivity(intent);
                //presenter.buscarPlaca();
            }
        });
    }

}
