package com.g2movil1_misiontic.preoperapp2.view.Geolocalizacion;

import android.content.Context;

public interface GeolocalizacionMVP {
    interface View {
        void getLocationPermision();
        Context getApplicationContext();

    }
    interface Model{

    }
    interface  Presenter{


    }
}
