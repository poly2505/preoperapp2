package com.g2movil1_misiontic.preoperapp2.view.Geolocalizacion;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.g2movil1_misiontic.preoperapp2.PerfilActivity;
import com.g2movil1_misiontic.preoperapp2.R;
import com.g2movil1_misiontic.preoperapp2.model.repository.LocationRepository;
import com.g2movil1_misiontic.preoperapp2.view.HomeConductor.HomeConductorActivity;
import com.g2movil1_misiontic.preoperapp2.view.InspCabina.FormInspCabinaActivity;
import com.g2movil1_misiontic.preoperapp2.view.mapa.MapaActivity;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GeolocalizacionActivity extends AppCompatActivity implements GeolocalizacionMVP.View, OnMapReadyCallback {

    private GeolocalizacionMVP.Presenter presenter;
    String placa;
    TextInputEditText GeoPlaca;
    TextView fechaCompleta;
    TextView hora;
    Button btnInspeccionar;
    LocationRepository repository;
 //   LatLng latLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geolocalizacion_conductor);
        presenter = new GeolocalizacionPresenter(this);
        repository = new LocationRepository(getApplicationContext());
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);


        Date d = new Date();
        //SACAMOS LA FECHA COMPLETA
        fechaCompleta = (TextView) findViewById(R.id.txtFechaCompleta);
        SimpleDateFormat fecc = new SimpleDateFormat("d, MMMM 'del' yyyy");
        String fechacComplString = fecc.format(d);
        fechaCompleta.setText(fechacComplString);
        //SACAMOS LA HORA
        hora = (TextView) findViewById(R.id.txtHora);
        SimpleDateFormat ho = new SimpleDateFormat("h:mm");
        String horaString = ho.format(d);
        hora.setText(horaString);


        btnInspeccionar = findViewById(R.id.btn_Inspeccionar2);
        btnInspeccionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GeoPlaca = findViewById(R.id.geoPlaca);
                placa = GeoPlaca.getText().toString().trim();

                Intent intent = new Intent(GeolocalizacionActivity.this, FormInspCabinaActivity.class);
                intent.putExtra("Fecha", fechacComplString);
                intent.putExtra("Hora", horaString);
                intent.putExtra("Placa", placa);
                startActivity(intent);
            }
        });


    }

    private void statusCheck() {

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage("Su GPS parece desactivado, ¿Desea habilitarlo?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", (dialog, id) -> startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                    .setNegativeButton("No", (dialog, id) -> {
                        GeolocalizacionActivity.this.onBackPressed();
                        dialog.cancel();
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        }
        //TODO recuerde que no creo unas funciones que hizo el profesor en la clase 3_ 1:42min

    }

    @Override
    public void getLocationPermision() {
        statusCheck();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION

            },1 );
            return ;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull @NotNull String[] permissions, @NonNull @NotNull int[] grantResults) {

        if (requestCode==1){
            for (int i = 0; i < grantResults.length; i++) {
                if(grantResults[i] != PackageManager.PERMISSION_GRANTED){

                    //Snackbar.make(null, "No tiene permisos suficientes para acceder a la app",Snackbar.LENGTH_SHORT).show();
                    Log.d("probandoGeo","Permisos de geolocalizacion NO exitosa");
                    return;
                }

            }
            //TODO notificar que ya tiene permisos
            //Snackbar.make(null, "_Permisos de geolocalizacion exitosa",Snackbar.LENGTH_SHORT).show();
            Log.d("probandoGeo","Permisos de geolocalizacion exitosa");
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        // TODO ¿Qué hacer con el mapa? Guardar la ubicación del conductor
        // googleMap.setMyLocationEnabled(true);
        //googleMap.isMyLocationEnabled();
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        repository.addLocationUpdateCallback(new LocationCallback() {

            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location loc = repository.getCurrentLocation();
                LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());

                googleMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("Aqui estoy!"));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            }
        });
    }


}