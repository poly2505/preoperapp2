package com.g2movil1_misiontic.preoperapp2.view.Supervisor;

import com.g2movil1_misiontic.preoperapp2.model.repository.SupervisorRepository;
import com.g2movil1_misiontic.preoperapp2.view.login.MainActivity;
import com.g2movil1_misiontic.preoperapp2.view.login.MainMVP;


public class SupervisorPresenter implements SupervisorMVP.Presenter {

    private final SupervisorMVP.View view;
    private final SupervisorMVP.Model model;

    public SupervisorPresenter (SupervisorMVP.View view){
        this.view = view;
        this.model = new SupervisorRepository();
    }


    @Override
    public void LogOut() {
        view.confirmLogOut();
    }

    public void logOutConfirmed(){
        model.logout();
        view.showActivity(MainActivity.class);
    }

}
