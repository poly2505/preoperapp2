package com.g2movil1_misiontic.preoperapp2.view.Supervisor;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.g2movil1_misiontic.preoperapp2.view.NewVehicle.CrearVehiculoActivity;
import com.g2movil1_misiontic.preoperapp2.PerfilActivity;
import com.g2movil1_misiontic.preoperapp2.R;
import com.g2movil1_misiontic.preoperapp2.view.ReportSearch.ReportActivity;
import com.g2movil1_misiontic.preoperapp2.view.UpdateList.UpdateListActivity;


public class HomeSupervisorActivity extends AppCompatActivity implements SupervisorMVP.View {

    private SupervisorMVP.Presenter presenter;
    Button btnUpdates;
    Button btnReports;
    Button btnCreateVehicle;
    Button btnProfile;

    private void initUI() {
        presenter = new SupervisorPresenter(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_supervisor);

        btnUpdates = findViewById(R.id.bt_update);
        btnUpdates.setOnClickListener(v -> {
            Intent intent = new Intent(HomeSupervisorActivity.this, UpdateListActivity.class);
            startActivity(intent);
        });

        btnReports = findViewById(R.id.bt_reports);
        btnReports.setOnClickListener(v -> {
            Intent intent = new Intent(HomeSupervisorActivity.this, ReportActivity.class);
            startActivity(intent);
        });
        btnCreateVehicle = findViewById(R.id.bt_crear_vehiculo);
        btnCreateVehicle.setOnClickListener(v -> {
            Intent intent = new Intent(HomeSupervisorActivity.this, CrearVehiculoActivity.class);
            startActivity(intent);
        });
        btnProfile = findViewById(R.id.bt_PerfilSuperv);
        btnProfile.setOnClickListener(v -> {
            Intent intent = new Intent(HomeSupervisorActivity.this, PerfilActivity.class);
            startActivity(intent);
        });

        initUI();


    }


    @Override
    public void onBackPressed() {
        presenter.LogOut();
    }

    public void confirmLogOut() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage("¿Está seguro de cerrar sesión?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        presenter.logOutConfirmed();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, type);
        startActivity(intent);
    }
}