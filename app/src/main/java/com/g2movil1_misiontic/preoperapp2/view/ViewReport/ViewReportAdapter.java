package com.g2movil1_misiontic.preoperapp2.view.ViewReport;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.g2movil1_misiontic.preoperapp2.R;
import com.g2movil1_misiontic.preoperapp2.model.repository.ChargeInfo;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;

public class ViewReportAdapter extends RecyclerView.Adapter<ViewReportAdapter.ViewHolder> {

    private List<ChargeInfo> data;
    private OnItemClickListener onItemClickListener;

    public ViewReportAdapter(List<ChargeInfo> data) {
        this.data = data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_charges, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewReportAdapter.ViewHolder holder, int position) {
        holder.setInfo(data.get(position));
       /* holder.getTvFecha().setText(data.get(position).getFecha());
        holder.getTvPlaca().setText(data.get(position).getPlaca());*/
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private MaterialTextView tvPlaca;
        private MaterialTextView tvFecha;

        private ChargeInfo info;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvFecha = itemView.findViewById(R.id.tv_address);
            tvPlaca = itemView.findViewById(R.id.tv_part);

            if (onItemClickListener != null) {
                itemView.setOnClickListener(this);
            }
        }

        public MaterialTextView getTvPlaca() {
            return tvPlaca;
        }

        public MaterialTextView getTvFecha() {
            return tvFecha;
        }

        public void setInfo(ChargeInfo info) {
            this.info = info;

            tvPlaca.setText(info.getPlaca());
            tvFecha.setText(info.getFecha());
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(info);
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ChargeInfo info);
    }
}

