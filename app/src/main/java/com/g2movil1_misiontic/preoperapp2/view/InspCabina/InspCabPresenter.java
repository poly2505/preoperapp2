package com.g2movil1_misiontic.preoperapp2.view.InspCabina;

import android.os.Bundle;

import com.g2movil1_misiontic.preoperapp2.view.Geolocalizacion.GeolocalizacionMVP;
import com.g2movil1_misiontic.preoperapp2.view.HomeConductor.HomeConductorActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class InspCabPresenter implements InspCabMVP.Presenter {
    private InspCabMVP.Model model;
    private InspCabMVP.View view;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private int novedad = 0;
    private String reportnew;
    private String dateNow;
    private String hourNow;
    private String placaNow;
    private String rbNibOk="";
    private String rbNibBad="";
    private String rbWhistleOk="";
    private String rbWhistleBad="";
    private String rbAlarmOk="";
    private String rbAlarmBad="";
    private String rbAuxWhistleOK="";
    private String rbAuxWhistleBad="";
    private String rbBeltOK="";
    private String rbBeltBad="";
    private String rbLightingOK="";
    private String rbLightingBad="";
    private String rbAirConditioningOk="";
    private String rbAirConditioningBad="";
    private String rbIndicatorsOK="";
    private String rbIndicatorsBad="";

    public InspCabPresenter(InspCabMVP.View view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void validateInspection() {
        InspCabInfo info = view.getInspCabInfo();

        //TODO revisar si esto debería estar en el modelo
        if(info.getNibsOK()){
            rbNibOk = "Buen estado";
        }

        if(info.getNibsBad()){
            rbNibBad = "Revisión";
            novedad = 1;
        }

        if(info.getWhistleOK()){
            rbWhistleOk = "Buen estado";
        }

        if(info.getWhistleBad()){
            rbWhistleBad = "Revisión";
            novedad = 1;
        }

        if(info.getAlarmOK()){
            rbAlarmOk = "Buen estado";
        }

        if(info.getAlarmBad()){
            rbAlarmBad = "Revisión";
            novedad = 1;
        }

        if(info.getAuxwhistleOK()){
            rbAuxWhistleOK = "Buen estado";
        }

        if(info.getAuxwhistleBad()){
            rbAuxWhistleBad = "Revisión";
            novedad = 1;
        }

        if(info.getBeltOK()){
            rbBeltOK = "Buen estado";
        }

        if(info.getBeltBad()){
            rbBeltBad = "Revisión";
            novedad = 1;
        }

        if(info.getLightingOK()){
            rbLightingOK = "Buen estado";
        }

        if(info.getLightingBad()){
            rbLightingBad = "Revisión";
            novedad = 1;
        }

        if(info.getAirConditioningOK()){
            rbAirConditioningOk = "Buen estado";
        }

        if(info.getAirConditioningBad()){
            rbAirConditioningBad = "Revisión";
            novedad = 1;
        }

        if(info.getIndicatorsOK()){
            rbIndicatorsOK = "Buen estado";
        }

        if(info.getIndicatorsBad()){
            rbIndicatorsBad = "Revisión";
            novedad = 1;
        }
        if(novedad==0){
            reportnew = "No presenta novedad";
        } else {
            reportnew = "Presenta Novedad";
        }

         SaveInspection();
    }

    private void SaveInspection() {


        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        Map<String, Object> data = new HashMap<>();
        data.put("Placa",placaNow);
        data.put("Novedad", reportnew);
        data.put("Fecha", dateNow);
        data.put("Hora", hourNow);
        data.put("Plumillas_limpiavidrios", rbNibOk+rbNibBad);
        data.put("Pito_Electrico", rbWhistleOk+rbWhistleBad);
        data.put("Alarma_de_testigo", rbAlarmOk+rbAlarmBad);
        data.put("Pito_Auxiliar", rbAuxWhistleOK+rbAuxWhistleBad);
        data.put("Cinturon_de_seguridad", rbBeltOK+rbBeltBad);
        data.put("Iluminacion_de_cabina", rbLightingOK+rbLightingBad);
        data.put("Aire_Acondicionado", rbAirConditioningOk+rbAirConditioningBad);
        data.put("Indicadores_luces_direccionales", rbIndicatorsOK+rbIndicatorsBad);


        mDatabase.child("Inspecciones").child(placaNow).child(dateNow+"_"+hourNow).setValue(data);

        view.successDialog();
    }

    public void backHome() {

        rbNibOk="";
        rbNibBad="";
        rbWhistleOk="";
        rbWhistleBad="";
        rbAlarmOk="";
        rbAlarmBad="";
        rbAuxWhistleOK="";
        rbAuxWhistleBad="";
        rbBeltOK="";
        rbBeltBad="";
        rbLightingOK="";
        rbLightingBad="";
        rbAirConditioningOk="";
        rbAirConditioningBad="";
        rbIndicatorsOK="";
        rbIndicatorsBad="";
        view.showActivity(HomeConductorActivity.class);
    }

    @Override
    public void saveDataGeo(String Date, String Hour, String Placa) {
        dateNow = Date;
        hourNow = Hour;
        placaNow = Placa;
    }

}
