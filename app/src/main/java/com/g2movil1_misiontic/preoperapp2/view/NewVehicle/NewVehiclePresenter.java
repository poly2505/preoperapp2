package com.g2movil1_misiontic.preoperapp2.view.NewVehicle;

import androidx.annotation.NonNull;

import com.g2movil1_misiontic.preoperapp2.view.HomeConductor.HomeConductorActivity;
import com.g2movil1_misiontic.preoperapp2.view.Register.Registerinfo;
import com.g2movil1_misiontic.preoperapp2.view.Supervisor.HomeSupervisorActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

public class NewVehiclePresenter implements NewVehicleMVP.Presenter{
    private final NewVehicleMVP.View view;
    private final NewVehicleMVP.Model model;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private String marca="";
    private String modelo="";
    private String placa="";
    private String poliza="";
    private String soat="";
    private String tecnicomecanica="";


    public NewVehiclePresenter(NewVehicleMVP.View view) {
        this.view = view;
        this.model = null;

    }

    public void ValidateNewregister() {
        InfoVehicle info = view.getRegisterVehicle();
        if (info.getMarca() == null || info.getMarca().trim().isEmpty()){
            view.ShowMarcaError("Ingrese una marca válida");
            return;
        }
        if (info.getModelo() == null || info.getModelo().trim().isEmpty()){
            view.ShowModeloError("Ingrese un modelo válido");
            return;
        }
        if (info.getPlaca() == null || info.getPlaca().trim().isEmpty()){
            view.ShowPlacaError("ingrese una placa válida");
            return;
        }

        if (info.getPoliza() == null || info.getPoliza().trim().isEmpty()){
            view.ShowPolizaError("Poliza no válida");
            return;
        }
        if (info.getSoat() == null || info.getSoat().trim().isEmpty()){
            view.ShowFechaSoatError("SOAT no válido");
            return;
        }
        if (info.getTecnicomecanica() == null || info.getTecnicomecanica().trim().isEmpty()){
            view.ShowfechaTecinomecanicaError("Tecnico Mecánica no válida");
            return;
        }

        NewVehicleRegister();


        //TODO faltaria hacer como se valida que el vehículo ya existe en la bd
    }

    @Override
    public void backHome() {
        view.showActivity(HomeSupervisorActivity.class);
    }

    public void NewVehicleRegister() {

        InfoVehicle info = view.getRegisterVehicle();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();


        marca = info.getMarca();
        modelo = info.getModelo();
        placa = info.getPlaca();
        poliza = info.getPoliza();
        soat = info.getSoat();
        tecnicomecanica = info.getTecnicomecanica();


            registerVehicle();

        view.ShowRegisterSuccess("El regístro se realizó con éxito");
    }

    private void registerVehicle() {

        Map<String, Object> data = new HashMap<>();
        data.put("Marca", marca);
        data.put("Modelo", modelo);
        data.put("Placa", placa);
        data.put("Poliza", poliza);
        data.put("SOAT", soat);
        data.put("TecnicoMecanica", tecnicomecanica);

        mDatabase.child("Vehicles").child(placa).setValue(data);

        view.successDialog();

    }

}
