package com.g2movil1_misiontic.preoperapp2.view.UpdateList;

public class UpdateListInfo {
    private String placa;
    private String driver;
    private String numberReport;

    public UpdateListInfo(String placa, String driver, String numberReport) {
        this.placa = placa;
        this.driver = driver;
        this.numberReport = numberReport;
    }

    public String getPlaca() {
        return placa;
    }
    public String getDriver() {
        return driver;
        }
    public String getNumberReport() {
        return numberReport;
    }
}
