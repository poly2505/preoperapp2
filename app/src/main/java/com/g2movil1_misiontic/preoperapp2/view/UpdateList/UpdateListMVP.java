package com.g2movil1_misiontic.preoperapp2.view.UpdateList;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

public interface UpdateListMVP {
    interface  Model {
        List<UpdateListInfo> loadReport();
    }
    interface  Presenter {

        void loadReport();

        void newReport();
    }
    interface  View {


        void loadReport(List<UpdateListInfo> updates);

        void showActivity(Class<? extends AppCompatActivity> type);
    }
}
