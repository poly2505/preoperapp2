package com.g2movil1_misiontic.preoperapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

import com.g2movil1_misiontic.preoperapp2.view.ReportSearch.ReportActivity;

public class ReportWithNewsActivity extends AppCompatActivity {

    Button btnBackReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_with_news);
        btnBackReport = findViewById(R.id.btn_BackReport);
        btnBackReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReportWithNewsActivity.this, ReportActivity.class);
                startActivity(intent);
            }
        });
    }
}